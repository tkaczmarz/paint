# Wielodostępowa aplikacja do malowania #

Aplikacja ta umożliwia malowanie na płótnie podobnie do aplikacji MS Paint. Dodatkowo wiele użytkowników może malować po tym samym płótnie równocześnie za pośrednictwem pliku do wymiany danych.

## Zasada działania ##

Program przyjmuje opcjonalny parametr oznaczający ścieżkę dostępu do pliku wymiany danych. Jeżeli nie zostanie podany brana pod uwagę jest ścieżka domyślna, czyli bieżący katalog.
Pierwszy użytkownik po uruchomieniu aplikacji tworzy plik 'share', jeżeli jeszcze taki nie istnieje. Po każdym wykonanym kroku rysowania do pliku dopisywana jest linia oznaczająca wykonaną akcję. 
Każda kolejna instancja programu po uruchomieniu odtwarza obraz z pliku wykonując po kolei wszystkie zapisane czynności.
Przed dopisaniem akcji do pliku program sprawdza, czy jest na bieżąco porównując numer linii, na której skończył ostatni odczyt z długością pliku.
Jeżeli nie sczytał wszystkich instrukcji to analizuje pozostałość i dopisuje na końcu swoją akcję.

Program został napisany w języku C z wykorzystaniem biblioteki Xlib.