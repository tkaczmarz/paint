// tutorial1: https://tronche.com/gui/x/xlib-tutorial/
// tutorial2: https://www.youtube.com/watch?v=NWYwDXN7b_s&list=PLypxmOPCOkHV4cwikC5_7Z981_EBfErS1&index=1
// informacje na temat pamięci współdzielonej XShm: https://www.x.org/releases/X11R7.7/doc/xextproto/shm.html
// polecenie kompilacji: gcc -o paint paint.c -lX11

#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define BUTTON_COUNT 22
#define key 898135468

typedef struct Button {
    int x, y;
    int width, height;
    char *text;
    unsigned long color;
} Button;

typedef struct Point {
    int x, y;
} Point;

typedef struct Register {
    Point buttonDown;
    Point buttonUp;
    int tool;
    int size;
    int color;
} Register;

enum Color {
    BLACK, WHITE, BUTTON, DARKGREY, RED, ORANGE, YELLOW,
    LIGHTGREEN, GREEN, DARKGREEN, CYAN, BLUE, PURPLE, PINK
};

// kolory
XColor colors[14];

// zmienne globalne
Button buttons[BUTTON_COUNT];
int control = -1;
const int fontSize = 6;
int color_chosen;
int mouseButtonPressed = 0;
int toolChosen = 1;
Point tmpPoint;
int size = 5;
FILE *file;
char filename[50] = "";
int line = 0;

    // prototypy funkcji \\
// ustawienie wartosci przycisku
Button SetButton(int xPos, int yPos, int w, int h, char *text, unsigned long color);
// inicjalizacja interfejsu uzytkownika 
void Init(Display *display, Colormap colormap);
// rysowanie interfejsu uzytkownika
void DrawUI(Display *display, GC gc, Window window, int screen);
// sprawdzenie, czy uzytkownik kliknal w przycisk
int CheckButtons(int x, int y);
// reagowanie na wcisniecie odpowiednich przyciskow
void Actions(Display *display, GC gc, Window window);
// rysowanie na plotnie
void Draw(Display *display, Window window, GC gc, int x, int y, int status, int toolChosen, int color_chosen, Bool save);
// wybranie koloru
void ChooseColor(Display *display, GC gc, int color);
// odswiezenie napisu rozmiaru narzedzia
void RefreshSizeText(Display *display, Window window, GC gc);
// ustawianie koloru aktywnego narzedzia
void SetToolButtonsColor();
// aktualizacja obrazu z pliku
int UpdateFromFile(Display *display, Window window, GC gc, int line);
// zapisanie instrukcji do pliku
int SaveToFile(Display *display, Window window, GC gc, int line, Register reg);
// tworzenie nowego wpisu
Register NewRegister(int x1, int y1, int x2, int y2, int tool, int size, int color);

int main(int argc, char *argv[])
{
    Display *mainDisplay;
    Window window, button1win;
    XSetWindowAttributes winAttributes;
    Visual *visual;
    GC gc;
    XGCValues gcValues;
    int screen, depth;

    // ustawienie sciezki pliku wymiany
    if (argc < 2)
        strcpy(filename, "share");
    else
    {
        strcpy(filename, argv[1]);
        strcat(filename, "/share");
    }

    // toolbox
    mainDisplay = XOpenDisplay(NULL);
    if (mainDisplay == NULL)
    {
        fprintf(stderr, "Cannot open display!\n");
        exit(1);
    }
    
    screen = DefaultScreen(mainDisplay);
    visual = DefaultVisual(mainDisplay, screen);
    depth = DefaultDepth(mainDisplay, screen);
    gc = DefaultGC(mainDisplay, screen);
    Colormap colormap = DefaultColormap(mainDisplay, screen);

    // inicjalizacja interfejsu
    Init(mainDisplay, colormap);

    winAttributes.background_pixel = XWhitePixel(mainDisplay, screen);
    winAttributes.override_redirect = False;

    // glowne okno
    window = XCreateWindow(mainDisplay, XRootWindow(mainDisplay, screen),
                            100, 100, 1024, 768, 1, depth, InputOutput, visual, 
                            CWBackPixel | CWOverrideRedirect, &winAttributes);

    XSelectInput(mainDisplay, window, ExposureMask | KeyPressMask | ButtonPressMask);
    XMapWindow(mainDisplay, window);
    
    // zablokowanie zmiany rozmiaru okna
    XSizeHints hints;
    hints.min_width = 1023;
    hints.max_width = 1024;
    hints.min_height = 767;
    hints.max_height = 768;
    XSetWMNormalHints(mainDisplay, window, &hints);

    // okno do rysowania
    Window canvasWindow = XCreateWindow(mainDisplay, window,
                            140, 0, 880, 740, 1, depth, InputOutput, visual,
                            CWBackPixel | CWOverrideRedirect, &winAttributes);

    XSelectInput(mainDisplay, canvasWindow, ExposureMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask);
    XMapWindow(mainDisplay, canvasWindow);

    // prawidlowe wylaczanie okna programu (ClientMessage event.)
    //Atom atom = XInternAtom(mainDisplay, "WM_DELETE_WINDOW", False);
    //XSetWMProtocols(mainDisplay, window, &atom, 1);
    
    char buffer[50] = "";
    char xString[5] = "";
    char yString[5] = "";

    XEvent event;
    // glowna petla programu
    while (control != 0)
    {
        XSetForeground(mainDisplay, gc, colors[color_chosen].pixel);    // reset koloru do wybranego
        // zdarzenia okna narzedzi
        XNextEvent(mainDisplay, &event);
        switch (event.type)
        {
            // event: wylaczenie okna
            //case ClientMessage:
            //control = 0;
            //break;

            // event: wyswietlenie okna
            case Expose:
            // rysowanie okna (przyciski itd.)
            DrawUI(mainDisplay, gc, window, screen);
            line = UpdateFromFile(mainDisplay, canvasWindow, gc, line);
            break;

            // event: klikniecie w okno
            case ButtonPress:      
	    if (event.xbutton.window == canvasWindow)   // wydarzenia dla rysowania
            {
                mouseButtonPressed = 1;
                Draw(mainDisplay, canvasWindow, gc, event.xbutton.x, event.xbutton.y, 1, toolChosen, color_chosen, True);
            }
            else if (event.xbutton.window == window)    // wydarzenia dla panelu narzedzi
            {
                control = CheckButtons(event.xbutton.x, event.xbutton.y);
                Actions(mainDisplay, gc, window);
            }    
            
            DrawUI(mainDisplay, gc, window, screen);
            break;

            // event: poruszenie myszy
            case MotionNotify:
            if (mouseButtonPressed == 1)
            {
                Draw(mainDisplay, canvasWindow, gc, event.xbutton.x, event.xbutton.y, 2, toolChosen, color_chosen, True);
            }
            // wyswietlenie wspolrzednych kursora
            if (event.xbutton.window == canvasWindow)
            {
                *buffer = '\0';
                *xString = '\0';
                *yString = '\0';
                sprintf(xString, "%d", event.xbutton.x);
                sprintf(yString, "%d", event.xbutton.y);
                strcpy(buffer, "Kursor: (");
                strncat(buffer, xString, strlen(xString));
                strncat(buffer, ", ", 2);
                strncat(buffer, yString, strlen(yString));
                strncat(buffer, ")", 1);
                XSetForeground(mainDisplay, gc, colors[WHITE].pixel);
                XFillRectangle(mainDisplay, window, gc, 10, 707 + (2 * fontSize), 120, fontSize * 2);
                XSetForeground(mainDisplay, gc, colors[BLACK].pixel);
                XDrawString(mainDisplay, window, gc, 10, 730, buffer, strlen(buffer));
                XSetForeground(mainDisplay, gc, colors[color_chosen].pixel);
            }
            line = UpdateFromFile(mainDisplay, canvasWindow, gc, line);
            break;

            // event: puszczenie przycisku myszy
            case ButtonRelease:
            mouseButtonPressed = 0;
            Draw(mainDisplay, canvasWindow, gc, event.xbutton.x, event.xbutton.y, 3, toolChosen, color_chosen, True);
            break;
        }
    }

    XCloseDisplay(mainDisplay);
    return 0;
}

Button SetButton(int xPos, int yPos, int w, int h, char *text, unsigned long color)
{
    Button button;
    button.x = xPos;
    button.y = yPos;
    button.width = w;
    button.height = h;
    button.text = text;
    button.color = color;
    return button;
}

void Init(Display *display, Colormap colormap)
{
    XParseColor(display, colormap, "rgb:00/00/00", &colors[0]);
    XAllocColor(display, colormap, &colors[0]);
    XParseColor(display, colormap, "rgb:FF/FF/FF", &colors[1]);
    XAllocColor(display, colormap, &colors[1]);
    XParseColor(display, colormap, "rgb:CC/CC/CC", &colors[2]);
    XAllocColor(display, colormap, &colors[2]);
    XParseColor(display, colormap, "rgb:88/88/88", &colors[3]);
    XAllocColor(display, colormap, &colors[3]);
    XParseColor(display, colormap, "rgb:FF/00/00", &colors[4]);
    XAllocColor(display, colormap, &colors[4]);
    XParseColor(display, colormap, "rgb:FF/8A/05", &colors[5]);
    XAllocColor(display, colormap, &colors[5]);
    XParseColor(display, colormap, "rgb:FF/E2/05", &colors[6]);
    XAllocColor(display, colormap, &colors[6]);
    XParseColor(display, colormap, "rgb:5B/FF/4C", &colors[7]);
    XAllocColor(display, colormap, &colors[7]);
    XParseColor(display, colormap, "rgb:00/FF/00", &colors[8]);
    XAllocColor(display, colormap, &colors[8]);
    XParseColor(display, colormap, "rgb:11/C9/00", &colors[9]);
    XAllocColor(display, colormap, &colors[9]);
    XParseColor(display, colormap, "rgb:00/FF/FF", &colors[10]);
    XAllocColor(display, colormap, &colors[10]);
    XParseColor(display, colormap, "rgb:00/00/FF", &colors[11]);
    XAllocColor(display, colormap, &colors[11]);
    XParseColor(display, colormap, "rgb:6A/0F/F9", &colors[12]);
    XAllocColor(display, colormap, &colors[12]);
    XParseColor(display, colormap, "rgb:FF/00/E7", &colors[13]);
    XAllocColor(display, colormap, &colors[13]);
    color_chosen = 0;

        // ustawianie przyciskow \\
    // przyciski narzedzi
    buttons[1] = SetButton(20, 35, 100, 40, "Pedzel", colors[BUTTON].pixel);
    buttons[2] = SetButton(20, 85, 100, 40, "Gumka", colors[BUTTON].pixel);
    buttons[3] = SetButton(20, 135, 100, 40, "Linia", colors[BUTTON].pixel);
    buttons[4] = SetButton(20, 185, 100, 40, "Elipsa", colors[BUTTON].pixel);
    buttons[5] = SetButton(20, 235, 100, 40, "Prostokat", colors[BUTTON].pixel);
    buttons[18] = SetButton(35, 465, 20, 30, "-", colors[BUTTON].pixel);
    buttons[19] = SetButton(85, 465, 20, 30, "+", colors[BUTTON].pixel);
    buttons[20] = SetButton(10, 465, 20, 30, "--", colors[BUTTON].pixel);
    buttons[21] = SetButton(110, 465, 20, 30, "++", colors[BUTTON].pixel);
    // przyciski kolorow
    buttons[6] = SetButton(10, 325, 25, 25, "", colors[BLACK].pixel);
    buttons[7] = SetButton(40, 325, 25, 25, "", colors[WHITE].pixel);
    buttons[8] = SetButton(70, 325, 25, 25, "", colors[RED].pixel);
    buttons[9] = SetButton(100, 325, 25, 25, "", colors[ORANGE].pixel);
    buttons[10] = SetButton(10, 355, 25, 25, "", colors[YELLOW].pixel);
    buttons[11] = SetButton(40, 355, 25, 25, "", colors[LIGHTGREEN].pixel);
    buttons[12] = SetButton(70, 355, 25, 25, "", colors[GREEN].pixel);
    buttons[13] = SetButton(100, 355, 25, 25, "", colors[DARKGREEN].pixel);
    buttons[14] = SetButton(10, 385, 25, 25, "", colors[CYAN].pixel);
    buttons[15] = SetButton(40, 385, 25, 25, "", colors[BLUE].pixel);
    buttons[16] = SetButton(70, 385, 25, 25, "", colors[PURPLE].pixel);
    buttons[17] = SetButton(100, 385, 25, 25, "", colors[PINK].pixel);

    buttons[0] = SetButton(20, 540, 100, 40, "Wyjscie", colors[RED].pixel);

    SetToolButtonsColor();
}

void DrawUI(Display *display, GC gc, Window window, int screen)
{
    // rysowanie elementow statycznych
    int xPos = 10, yPos = 10;
    XSetForeground(display, gc, colors[BLACK].pixel);
    XDrawString(display, window, gc, xPos, yPos += fontSize, "Narzedzia", 9);
    XDrawLine(display, window, gc, xPos = 0, yPos += 5, 140, yPos);

    XDrawString(display, window, gc, xPos = 10, yPos = 300, "Kolory", 6);
    XSetForeground(display, gc, colors[DARKGREY].pixel);
    XFillRectangle(display, window, gc, 100, 285, 25, 25);
    XSetForeground(display, gc, colors[color_chosen].pixel);    // wybrany kolor
    XFillRectangle(display, window, gc, 101, 286, 22, 22);
    XSetForeground(display, gc, colors[BLACK].pixel);
    XDrawLine(display, window, gc, xPos = 0, yPos += 15, 140, yPos);

    XDrawString(display, window, gc, xPos = 10, yPos = 450, "Rozmiar narzedzia", 17);
    XDrawLine(display, window, gc, xPos = 0, yPos += 5, 140, yPos);

    RefreshSizeText(display, window, gc);

    // rysowanie przyciskow
    int i;
    for (i = 0; i < BUTTON_COUNT; i++)
    {
        // narysowanie ramki przycisku
        XSetForeground(display, gc, colors[DARKGREY].pixel);
        XFillRectangle(display, window, gc, buttons[i].x, buttons[i].y,
                                            buttons[i].width, buttons[i].height);

        // narysowanie wnetrza przycisku
        XSetForeground(display, gc, buttons[i].color);
        XFillRectangle(display, window, gc, buttons[i].x + 1, buttons[i].y + 1,
                                            buttons[i].width - 3, buttons[i].height - 3);

        // wyswietlenie tekstu w odpowiednim kolorze na przycisku
        XSetForeground(display, gc, colors[BLACK].pixel);
        int x, y;
        // wysrodkowanie tekstu w poziomie
        x = buttons[i].x + (buttons[i].width / 2) - (strlen(buttons[i].text) * fontSize / 2);
        // wysrodkowanie tekstu w pionie
        y = buttons[i].y + (buttons[i].height / 2) + (fontSize / 2);
        XDrawString(display, window, gc, x, y, buttons[i].text, strlen(buttons[i].text));
    }

    XFlush(display); // wyswietlenie zmian
}

int CheckButtons(int x, int y)
{
    int i;
    for (i = 0; i < BUTTON_COUNT; i++)
    {
        // jezeli punkt klikniecia znajduje sie w obszarze przycisku
        if (buttons[i].x <= x && (buttons[i].x + buttons[i].width) >= x &&
            buttons[i].y <= y && (buttons[i].y + buttons[i].height) >= y)
        {
            return i;
        }
    }

    return -1;
}

void Actions(Display *display, GC gc, Window window)
{
    // obsluga przyciskow wg ID
    switch (control)
    {
        // przyciski wyboru narzedzi
        case 1:
        toolChosen = 1; // pedzel
        SetToolButtonsColor();
        break;
        case 2:
        toolChosen = 2; // gumka
        SetToolButtonsColor();
        break;
        case 3:
        toolChosen = 3; // linia
        SetToolButtonsColor();
        break;
        case 4:
        toolChosen = 4; // elipsa
        SetToolButtonsColor();
        break;
        case 5:
        toolChosen = 5; // prostokat
        SetToolButtonsColor();
        break;
        // przyciski wybierania kolorow
        case 6:
        ChooseColor(display, gc, BLACK);
        break;
        case 7:
        ChooseColor(display, gc, WHITE);
        break;
        case 8:
        ChooseColor(display, gc, RED);
        break;
        case 9:
        ChooseColor(display, gc, ORANGE);
        break;
        case 10:
        ChooseColor(display, gc, YELLOW);
        break;
        case 11:
        ChooseColor(display, gc, LIGHTGREEN);
        break;
        case 12:
        ChooseColor(display, gc, GREEN);
        break;
        case 13:
        ChooseColor(display, gc, DARKGREEN);
        break;
        case 14:
        ChooseColor(display, gc, CYAN);
        break;
        case 15:
        ChooseColor(display, gc, BLUE);
        break;
        case 16:
        ChooseColor(display, gc, PURPLE);
        break;
        case 17:
        ChooseColor(display, gc, PINK);
        break;
        // przyciski sterowania rozmiarem narzedzi
        case 18:
        if (size > 1)
        {
            size--;
        }
        break;
        case 19:
        if (size < 99)
        {
            size++;
        }
        break;
        case 20:
        if (size - 10 <= 1)
            size = 1;
        else
            size -= 10;
        break;
        case 21:
        if (size + 10 >= 99)
            size = 99;
        else
            size += 10;
        break;

        default:
        if (control >= 0 && control < BUTTON_COUNT)
            printf("Wcisnieto przycisk %s\n", buttons[control].text);
        break;   
    }
}

void ChooseColor(Display *display, GC gc, int color)
{
    color_chosen = color;
    XSetForeground(display, gc, colors[color_chosen].pixel);
}

void Draw(Display *display, Window window, GC gc, int x, int y, int status, int toolChosen, int color, Bool save)
{
    // obsluga roznych narzedzi
    switch (toolChosen)
    {
        case 1:     // pedzel
        XSetForeground(display, gc, colors[color].pixel);
        XFillRectangle(display, window, gc, x - (size / 2), y - (size / 2), size, size);
        XSetForeground(display, gc, colors[color_chosen].pixel);

        if (save)
            line = SaveToFile(display, window, gc, line, NewRegister(x, y, x, y, toolChosen, size, color));
        break;

        case 2:     // gumka
        XSetForeground(display, gc, colors[WHITE].pixel);
        XFillRectangle(display, window, gc, x - (size / 2), y - (size / 2), size, size);
        XSetForeground(display, gc, colors[color_chosen].pixel);

        if (save)
            line = SaveToFile(display, window, gc, line, NewRegister(x, y, x, y, toolChosen, size, WHITE));
        break;

        case 3:     // linia
        if (status == 1)    // wcisniety przycisk myszy
        {
            tmpPoint.x = x;
            tmpPoint.y = y;
        }
        else if (status == 3)   // odpuszczony przycisk myszy
        {
            XSetForeground(display, gc, colors[color].pixel);
            
            XPoint points[4];
            points[0].x = tmpPoint.x - (size / 2);
            points[0].y = tmpPoint.y - (size / 2);
            points[1].x = tmpPoint.x + (size / 2);
            points[1].y = tmpPoint.y + (size / 2);
            points[2].x = x + (size / 2);
            points[2].y = y + (size / 2);
            points[3].x = x - (size / 2);
            points[3].y = y - (size / 2);
            XFillPolygon(display, window, gc, points, 4, Complex, CoordModeOrigin);
            XSetForeground(display, gc, colors[color_chosen].pixel);

            if (save)
                line = SaveToFile(display, window, gc, line, NewRegister(tmpPoint.x, tmpPoint.y, x, y, toolChosen, size, color));
        }
        break;

        case 4:     // elipsa
        if (status == 1)    // wcisniety przycisk myszy
        {
            tmpPoint.x = x;
            tmpPoint.y = y;
        }
        else if (status == 3)   // odpuszczony przycisk myszy
        {
            XSetForeground(display, gc, colors[color].pixel);
            if (tmpPoint.x >= x)
            {
                if (tmpPoint.y >= y)
                {
                    XFillArc(display, window, gc, x, y, abs(x - tmpPoint.x), abs(y - tmpPoint.y), 23040, 23040);
                }
                else if (tmpPoint.y < y)
                {
                    XFillArc(display, window, gc, x, tmpPoint.y, abs(x - tmpPoint.x), abs(y - tmpPoint.y), 23040, 23040);
                }
            }
            else if (tmpPoint.x < x)
            {
                if (tmpPoint.y >= y)
                {
                    XFillArc(display, window, gc, tmpPoint.x, y, abs(x - tmpPoint.x), abs(y - tmpPoint.y), 23040, 23040);
                }
                else if (tmpPoint.y < y)
                {
                    XFillArc(display, window, gc, tmpPoint.x, tmpPoint.y, abs(x - tmpPoint.x), abs(y - tmpPoint.y), 23040, 23040);
                }
            }
            XSetForeground(display, gc, colors[color_chosen].pixel);

            if (save)
                line = SaveToFile(display, window, gc, line, NewRegister(tmpPoint.x, tmpPoint.y, x, y, toolChosen, size, color));
        }
        break;

        case 5:     // prostokat
        if (status == 1)    // wcisniety przycisk myszy
        {
            tmpPoint.x = x;
            tmpPoint.y = y;
        }
        else if (status == 3)   // odpuszczony przycisk myszy
        {
            XSetForeground(display, gc, colors[color].pixel);
            if (tmpPoint.x >= x)
            {
                if (tmpPoint.y >= y)
                {
                    XFillRectangle(display, window, gc, x, y, abs(x - tmpPoint.x), abs(y - tmpPoint.y));
                }
                else if (tmpPoint.y < y)
                {
                    XFillRectangle(display, window, gc, x, tmpPoint.y, abs(x - tmpPoint.x), abs(y - tmpPoint.y));
                }
            }
            else if (tmpPoint.x < x)
            {
                if (tmpPoint.y >= y)
                {
                    XFillRectangle(display, window, gc, tmpPoint.x, y, abs(x - tmpPoint.x), abs(y - tmpPoint.y));
                }
                else if (tmpPoint.y < y)
                {
                    XFillRectangle(display, window, gc, tmpPoint.x, tmpPoint.y, abs(x - tmpPoint.x), abs(y - tmpPoint.y));
                }
            }
            XSetForeground(display, gc, colors[color_chosen].pixel);

            if (save)
                line = SaveToFile(display, window, gc, line, NewRegister(tmpPoint.x, tmpPoint.y, x, y, toolChosen, size, color));
        }
        break;
    }
}

void RefreshSizeText(Display *display, Window window, GC gc)
{
    char sizeText[5] = "";
    sprintf(sizeText, "%d", size);
    XSetForeground(display, gc, colors[WHITE].pixel);
    XFillRectangle(display, window, gc, 65, 470, 15, 15);
    XSetForeground(display, gc, colors[BLACK].pixel);
    XDrawString(display, window, gc, 64 + (fontSize / 2), 470 + (2 * fontSize), sizeText, strlen(sizeText));
    XSetForeground(display, gc, colors[color_chosen].pixel);
}

void SetToolButtonsColor()
{
    int i;
    for (i = 1; i <= 5; i++)
    {
        if (toolChosen == i)
            buttons[i].color = colors[YELLOW].pixel;
        else
            buttons[i].color = colors[BUTTON].pixel;
    }
}

int UpdateFromFile(Display *display, Window window, GC gc, int line)
{
    int i = 0;
    char buff[50];
    Register reg;

    file = fopen(filename, "r");

    if (file != NULL)
    {
        while (i < line)
        {
            fgets(buff, sizeof(buff), file);
            i++;
        }
        int s = size;
        // czytanie pliku liniami
        while (fgets(buff, sizeof(buff), file) != NULL)
        {
            i++;

            sscanf(buff, "%d %d %d %d %d %d %d", &reg.buttonDown.x, &reg.buttonDown.y,
                    &reg.buttonUp.x, &reg.buttonUp.y, &reg.tool, &reg.size, &reg.color);
            size = reg.size;
            if (reg.tool <= 2)
            {
                Draw(display, window, gc, reg.buttonDown.x, reg.buttonDown.y, 1, reg.tool, reg.color, False);
            }
            else
            {
                Draw(display, window, gc, reg.buttonDown.x, reg.buttonDown.y, 1, reg.tool, reg.color, False);
                Draw(display, window, gc, reg.buttonUp.x, reg.buttonUp.y, 3, reg.tool, reg.color, False);
            }
        }
        size = s;
        fclose(file);
        return i;
    }
    else
    {
        file = fopen(filename, "a+");
        if (file == NULL)
        {
            fprintf(stderr, "Can't open/create share file!\n");
            exit(1);
        }
        else
            fclose(file);
    }

    return 0;
}

int SaveToFile(Display *display, Window window, GC gc, int line, Register reg)
{
    line = UpdateFromFile(display, window, gc, line);

    char buff[50];

    file = fopen(filename, "a");

    if (file != NULL) 
    {
        fprintf(file, "%d %d %d %d %d %d %d\n", reg.buttonDown.x, reg.buttonDown.y,
                    reg.buttonUp.x, reg.buttonUp.y, reg.tool, reg.size, reg.color);
    }
    else
    {
        fprintf(stderr, "Can't open share file!\n");
        exit(1);
    }
    fclose(file);

    return line + 1;
}

Register NewRegister(int x1, int y1, int x2, int y2, int tool, int size, int color)
{
    Register reg;

    reg.buttonDown.x = x1;
    reg.buttonDown.y = y1;
    reg.buttonUp.x = x2;
    reg.buttonUp.y = y2;
    reg.tool = tool;
    reg.size = size;
    reg.color = color;

    return reg;
}