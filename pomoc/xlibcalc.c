/* Compilation command:
 * gcc -o xlibcalc xlibcalc.c -lX11 -Wall
 */

#include <X11/Xlib.h>
#include <stdio.h>

#define X_BLOCKS 4
#define Y_BLOCKS 6
#define SPACING 5



static const char calc_button[5][4] = {
  {'C', '/', '*', '-'},
  {'7', '8', '9', '+'},
  {'4', '5', '6', ' '},
  {'1', '2', '3', ' '},
  {'0', ' ', '.', '='},
};

static double calc_display_value = 0.0;
static double calc_buffer_value = 0.0;
static char calc_operation = '\0';
static int calc_decimal_mode = 0;
static int calc_display_tainted = 0;



static int power(int x, int y)
{
  int n;
  for (n = 1; y > 0; y--)
    n *= x;
  return n;
}



static void calc_action(char operation)
{
  switch (operation) {
  case 0x08: /* backspace */
  case 'C':
  case 'c':
    calc_display_value = 0.0;
    calc_buffer_value = 0.0;
    calc_decimal_mode = 0;
    calc_operation = '\0';
    calc_display_tainted = 0;
    break;

  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    if (calc_display_tainted) {
      calc_display_value = 0.0;
      calc_display_tainted = 0;
    }
    if (calc_decimal_mode) {
      /* Resolution only shows up to 6 decimals, so no reason to allow more. */
      if (calc_decimal_mode <= 6) {
        calc_display_value += ((operation - 0x30) /
          (double)power(10, calc_decimal_mode));
        calc_decimal_mode++;
      }
    } else {
      calc_display_value *= 10;
      calc_display_value += (operation - 0x30);
    }
    break;

  case ',':
  case '.':
    if (calc_decimal_mode == 0)
      calc_decimal_mode = 1;
    break;

  case '+':
  case '-':
  case '*':
  case '/':
    if (calc_operation != '\0') {
      switch (calc_operation) {
      case '+':
        calc_display_value = calc_buffer_value + calc_display_value;
        break;
      case '-':
        calc_display_value = calc_buffer_value - calc_display_value;
        break;
      case '*':
        calc_display_value = calc_buffer_value * calc_display_value;
        break;
      case '/':
        calc_display_value = calc_buffer_value / calc_display_value;
        break;
      }
    }
    calc_buffer_value = calc_display_value;
    calc_operation = operation;
    calc_display_tainted = 1;
    calc_decimal_mode = 0;
    break;

  case 0x0d: /* enter */
  case '=':
    if (calc_operation != '\0') {
      switch (calc_operation) {
      case '+':
        calc_display_value = calc_buffer_value + calc_display_value;
        break;
      case '-':
        calc_display_value = calc_buffer_value - calc_display_value;
        break;
      case '*':
        calc_display_value = calc_buffer_value * calc_display_value;
        break;
      case '/':
        calc_display_value = calc_buffer_value / calc_display_value;
        break;
      }
    }
    calc_operation = '\0';
    calc_display_tainted = 1;
    calc_decimal_mode = 0;
    break;

  }
}



static void calc_draw(Display *display, Window window, int screen)
{
  int i, j, buf_len;
  char buf[32];
  XWindowAttributes attributes;
  GC gc;

  if (XGetWindowAttributes(display, window, &attributes) == 0)
    return;
  gc = DefaultGC(display, screen);
  
  /* Draw screen. */
  XClearArea(display, window, SPACING, SPACING,
    attributes.width - (SPACING * 2),
    (attributes.height - ((Y_BLOCKS - 1 + 2) * SPACING)) / Y_BLOCKS,
    False);
  XDrawRectangle(display, window, gc, SPACING, SPACING,
    attributes.width - (SPACING * 2),
    (attributes.height - ((Y_BLOCKS - 1 + 2) * SPACING)) / Y_BLOCKS);
  
  buf_len = snprintf(buf, 32, "%f", calc_display_value);
  XDrawString(display, window, gc, 30, 30, buf, buf_len); 

  /* Draw buttons with this loop. */
  for (i = 0; i < X_BLOCKS; i++) {
    for (j = 1; j < Y_BLOCKS; j++) {
      XDrawRectangle(display, window, gc, 
        (((attributes.width - (X_BLOCKS - 1)) / X_BLOCKS) * i) + SPACING,
        (((attributes.height - (Y_BLOCKS - 1)) / Y_BLOCKS) * j) + SPACING,
        (attributes.width - ((X_BLOCKS - 1 + 2) * SPACING)) / X_BLOCKS,
        (attributes.height - ((Y_BLOCKS - 1 + 2) * SPACING)) / Y_BLOCKS);
      
      XDrawString(display, window, gc, 
        ((attributes.width / X_BLOCKS) * (i + 1)) - 
        attributes.width / X_BLOCKS / 2,
        ((attributes.height / Y_BLOCKS) * (j + 1)) - 
        attributes.height / Y_BLOCKS / 2,
        &calc_button[j - 1][i], 1);
    }
  }
}



void calc_button_sense(Display *display, Window window, int x, int y)
{
  int i, j, x1, x2, y1, y2;
  XWindowAttributes attributes;

  if (XGetWindowAttributes(display, window, &attributes) == 0)
    return;

  for (i = 0; i < X_BLOCKS; i++) {
    for (j = 1; j < Y_BLOCKS; j++) {
      x1 = (((attributes.width - (X_BLOCKS - 1)) / X_BLOCKS) * i) + SPACING;
      y1 = (((attributes.height - (Y_BLOCKS - 1)) / Y_BLOCKS) * j) + SPACING;
      x2 = x1 + (attributes.width - ((X_BLOCKS - 1 + 2) * SPACING)) / X_BLOCKS;
      y2 = y1 + (attributes.height - ((Y_BLOCKS - 1 + 2) * SPACING)) / Y_BLOCKS;
      if (x > x1 && x < x2 && y > y1 && y < y2)
        calc_action(calc_button[j - 1][i]);
    }
  }
}



int main(void)
{
  Display *display;
  Window window;
  XEvent event;
  Atom atom;
  KeySym key;
  int screen, done;
 
  display = XOpenDisplay(NULL); /* NULL = Use DISPLAY environment variable. */
  if (display == NULL) {
    fprintf(stderr, "Cannot open display.");
    return 1;
  }

  screen = XDefaultScreen(display);
  window = XCreateSimpleWindow(display, XRootWindow(display, screen), 0, 0,
    200, 300, 3, XBlackPixel(display, screen), XWhitePixel(display, screen));

  XSelectInput(display, window, ExposureMask | KeyPressMask | ButtonPressMask);
  XMapWindow(display, window);

  /* Setup proper delete window handling. (Get ClientMessage event.) */
  atom = XInternAtom(display, "WM_DELETE_WINDOW", False);
  XSetWMProtocols(display, window, &atom, 1);

  done = 0;
  while (! done) {
    XNextEvent(display, &event);
    switch (event.type) {
    case ClientMessage:
      /* This seems to be the way to detect the delete message... */
      if (event.xclient.data.s[0] == atom)
        done = 1;
      break;

    case Expose:
      calc_draw(display, window, screen);
      break;

    case KeyPress:
      key = XKeycodeToKeysym(display, event.xkey.keycode, 0);
      if (key == 'q')
        done = 1;
      else
        calc_action(key);
        event.type = Expose; /* Order a redraw of the window. */
        XSendEvent(display, window, False, 0, &event);
      break;

    case ButtonPress:
      calc_button_sense(display, window, event.xbutton.x, event.xbutton.y);
      printf("x: %i, y: %i\n", event.xbutton.x, event.xbutton.y);
      event.type = Expose;
      XSendEvent(display, window, False, 0, &event);
      break;
    }
  }

  XCloseDisplay(display);
  return 0;
}

